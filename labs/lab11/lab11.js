export class Lab11{
  
  testDefaultParameters(firstParam,secondParam = 100){
   	return{
     	first:firstParam,
        second:secondParam
    };
  }
  
  testTemplateLiterals(firstName,middleName,lastName){
    return `${firstName},${middleName},${lastName}`;
  }
  
  testMultilineString(){
    return `Line1
Line2
Line3
Line4`;
  }
  
  testSortWithArrowFunction(unsorted) {
    return unsorted.sort((a,b) => b - a);
  }

}

let lab11 = new Lab11();

let defaultParamResult = lab11.testDefaultParameters('Value');
console.log(defaultParamResult.first);
console.log(defaultParamResult.second);

let overrideParamResult = lab11.testDefaultParameters('Value 1', 250);
console.log(overrideParamResult.first);
console.log(overrideParamResult.second);

console.log(lab11.testTemplateLiterals('I','like','cake'));

console.log(lab11.testMultilineString());

  let numbers = [10, 3, 5, 20, 1, 30, 4, 6, 8];
  console.log(numbers);
  let sorted = lab11.testSortWithArrowFunction(numbers);
  console.log(numbers);
  console.log(sorted);